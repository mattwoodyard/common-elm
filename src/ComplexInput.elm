module ComplexInput exposing (..)
import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


import Json.Decode
import Json.Encode
import String
import Semantic as S

type Editable a
  = Value a
  | ActiveEdit String (Maybe a)
  | DecodeError String String

type ValidatedInput origT
  = Original origT
  | Modified {original: origT, new: origT}
  | Editting origT String
  | ParseError {original: origT, input_value: String, error: String}

type ValidationTime = ValidOnBlur | ValidOnSet

type alias Context origT =
  { render: origT -> String
  , parse: String -> Result String origT
  , validateWhen: ValidationTime
  }

type Msg
  = Set String
  | Blur


textbox' v =
  textarea [value v, onInput Set, onBlur Blur] []
 
input' v =
  input [value v, onInput Set, onBlur Blur] []

-- Convert an input to a result
toResult : Context origT -> ValidatedInput origT -> Result String origT
toResult ctx model =
  case model of
    Original v -> Ok v
    Modified m -> Ok m.new
    Editting orig cur ->
      case ctx.parse cur of
        Err e -> Err e
        Ok v -> Ok v
    ParseError err -> Err err.error




viewTextBox : Context origT -> ValidatedInput origT -> Html Msg
viewTextBox ctx model =
  case model of
    Original v -> textbox' (ctx.render v) 
    Modified m -> textbox' (ctx.render m.new)
    Editting orig cur -> textbox' cur
    ParseError err -> textbox' err.input_value




viewInput : Context origT -> ValidatedInput origT -> Html Msg
viewInput ctx model =
  case model of
    Original v -> input' (ctx.render v) 
    Modified m -> input' (ctx.render m.new)
    Editting orig cur -> input' cur
    ParseError err -> input' err.input_value


liftInput: (Msg -> a) -> Context origT -> ValidatedInput origT -> Html a
liftInput wrap ctx model=
  Html.App.map wrap (viewInput ctx model)



liftTextbox: (Msg -> a) -> Context origT -> ValidatedInput origT -> Html a
liftTextbox wrap ctx model=
  Html.App.map wrap (viewTextBox ctx model)




    

update : Context origT -> Msg -> ValidatedInput origT -> ValidatedInput origT
update ctx msg model =
  case (msg, model) of
    (Set s, Original v) -> Editting v s
    (Set s, Modified mod) -> Editting mod.original s
    (Set s, Editting orig txt) -> Editting orig s
    (Set s, ParseError err) -> Editting err.original s
    (Blur, Original v) -> Original v
    (Blur, Modified mod) -> Modified mod
    (Blur, Editting orig txt) ->
      case ctx.parse txt of
        Ok v -> Modified {original = orig, new = v}
        Err ev -> ParseError {original = orig, input_value = txt, error =ev }

    (Blur, ParseError _) ->  model


rawTextInput = { render = identity, parse = Ok, validateWhen = ValidOnBlur }

intInput = { render = toString, parse = String.toInt, validateWhen = ValidOnBlur }
floatInput = { render = toString, parse = String.toFloat, validateWhen = ValidOnBlur }



updateEditable : (String -> Result String a) -> Msg -> Editable a -> Editable a
updateEditable decode msg model =
  case msg of
    Set s -> ActiveEdit s Nothing
    Blur ->
      case model of
        Value v -> Value v
        ActiveEdit s v ->
          case decode s of
            Ok v -> Value v
            Err e -> DecodeError s e
        DecodeError s e ->
          DecodeError s e 


editInput:  (origT -> String) -> Editable origT -> Html Msg
editInput encode model =
  case model of
    Value v ->
      input' (encode v)
    ActiveEdit s _ ->
      input' s
    DecodeError s e ->
      input' s


withInput: (Msg -> a) -> (origT -> String) -> Editable origT -> Html a
withInput wrap ctx model=
  Html.App.map wrap (editInput ctx model)


withTextarea: (Msg -> a) -> (origT -> String) -> Editable origT -> Html a
withTextarea wrap encode model =
  case model of
    Value v ->
      Html.App.map wrap <| textbox' (encode v)
    ActiveEdit s _ ->
      Html.App.map wrap <| textbox' s
    DecodeError s e ->
      Html.App.map wrap <| textbox' s



quickField: ((Msg -> a) -> (origT -> String) -> Editable origT -> Html a) ->
            S.FieldLabel ->
            (Msg -> a) ->
            (origT -> String) ->
            Editable origT ->
            Html a
quickField withChild label wrap encode model =
  let
    emsg = case model of
             DecodeError _ e -> Just e
             _ -> Nothing
    fld = { label = label
          , error_text = emsg
          , content = withChild wrap encode model
          , width = Nothing
          }
  in
    S.viewField fld


stringField lbl updfn value = quickField withInput lbl updfn identity value
floatField lbl updfn value = quickField withInput lbl updfn toString value
inputField = quickField withInput
textareaField = quickField withTextarea


editableToString: (a -> String) -> Editable a -> String
editableToString encode e =
  case e of
    Value v -> encode v
    ActiveEdit s _ -> s
    DecodeError s _ -> s


asText : (a -> String) -> Editable a -> Html b
asText encode e =
  case e of
    Value v -> text (encode v)
    ActiveEdit s _ -> text s
    DecodeError s _ -> text s


fromJson dec = Json.Decode.map Value dec


toJson jsonenc v =
  case v of
    Value v' -> jsonenc v'
    _ -> Json.Encode.null
 

editableToResult v =
  case v of
    Value v' -> Ok v'
    _ -> Err "invalid"


maybeResult d v =
  Maybe.withDefault (Ok d) (Maybe.map editableToResult v)
