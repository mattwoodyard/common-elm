module SimpleTableCrud exposing (..)



import PostgrestHelpers as PG




type alias TableContext origT editT msgT =
  { columns: List (String, (origT -> Html msgT))
  , beginEdit: origT -> editT
  , saveEdit: editT -> Result String origT
  , dbSave: origT -> Cmd msgT
  , dbList: Cmd msgT
  , dbGetOne: PG.IntPk -> Maybe origT
  }

type alias CrudTable origT editT msgT =
  {

  }









