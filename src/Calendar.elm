module Calendar exposing (..)




import Http
import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

import Json.Decode
import Json.Encode
import Json.Decode exposing ((:=))

import PostgrestHelpers as Pg

import Util as U
-- import FKLookup as FKL
-- import TextInput as TI
import Task


import Tuple
import CmdTuple

import Date exposing (..)
import List.Extra exposing (groupWhile)
-- import Native.DateMissing

import Dict
import Date.Extra as D


-- type alias IDate = {year: Int, month: Month, day: Int}

type alias Context a =
  { week: (List (Html a) -> Html a)
  , day: (Date -> Bool -> Html a -> Html a)
  , month: (Date -> List (Html a) -> Html a)
  , highlight: (Month -> Int -> Int -> Bool)
  , dayOfWeek: (Day -> Int)
  , default: Html a
  }

{-
Compute the year ordinal for a Month and day

Wikipedia
To the day of 	Jan 	Feb 	Mar 	Apr 	May 	Jun 	Jul 	Aug 	Sep 	Oct 	Nov 	Dec
        Add 	0 	31 	59 	90 	120 	151 	181 	212 	243 	273 	304 	334
Leap years 	0 	31 	60 	91 	121 	152 	182 	213 	244 	274 	305 	335
-}
dayOrdinal: Month -> Int -> Int -> Int
dayOrdinal month day year = 
  let
    leap =  if isLeap year then 1 else 0
    mday = day 
  in
    case month of
      Jan -> mday
      Feb -> 31 + mday
      Mar -> 59 + mday + leap
      Apr -> 90 + mday + leap
      May -> 120 + mday + leap
      Jun -> 151 + mday + leap
      Jul -> 181 + mday + leap
      Aug -> 212 + mday + leap
      Sep -> 243 + mday + leap
      Oct -> 273 + mday + leap
      Nov -> 304 + mday + leap
      Dec -> 334 + mday + leap

dateOrdinal: Date -> Int
dateOrdinal date =
  dayOrdinal (month date) (day date) (year date)

-- TODO better names
weekDayToIntUS: Day -> Int
weekDayToIntUS day =
  case day of
    Sun -> 0
    Mon -> 1
    Tue -> 2
    Wed -> 3
    Thu -> 4
    Fri -> 5
    Sat -> 6

weekDayToIntISO: Day -> Int
weekDayToIntISO day =
  case day of
    Mon -> 1
    Tue -> 2
    Wed -> 3
    Thu -> 4
    Fri -> 5
    Sat -> 6
    Sun -> 7

weekDayToIntISO0 x = weekDayToIntISO x - 1


isLeap yr = (yr % 4 == 0 && (yr % 100 /= 0)) || yr % 400 == 0

daysInMonth: Date -> Int
daysInMonth date =
  case month date of
    Jan -> 31
    Feb  -> if isLeap (year date) then 29 else 28
    Mar  -> 31
    Apr  -> 30
    May  -> 31
    Jun  -> 30
    Jul  -> 31
    Aug  -> 31
    Sep  -> 30
    Oct  -> 31
    Nov  -> 30
    Dec -> 31


monthInt: Month -> Int
monthInt month =
  case month of
    Jan -> 0
    Feb  -> 1
    Mar  -> 2
    Apr  -> 3
    May  ->  4
    Jun  -> 5
    Jul  -> 6
    Aug  -> 7
    Sep  -> 8
    Oct  -> 9
    Nov  -> 10
    Dec -> 11




weekNumber: Date -> Int
weekNumber date =
  case (Date.dayOfWeek date) of
    Sun -> D.weekNumber date + 1
    _ -> D.weekNumber date



simpleTableWeek: List (Html a) -> Html a
simpleTableWeek week =
  tr [] week

simpleTableDay: Date -> Bool -> Html a -> Html a
simpleTableDay daynum highlight day_content =
  td
    []
    [ span [] [text (toString (day daynum))]
    , day_content
    ]

simpleStandardMonth: Date -> List (Html a) -> Html a
simpleStandardMonth monthV weeks =
  let
    mstring = (toString (month monthV)) ++ " " ++ (toString (year monthV))
  in
    div
      []
      [ div [] [text mstring]
      , table [] weeks
      ]


clickableTableDay: (Date -> a) -> Date -> Bool -> Html a -> Html a
clickableTableDay clickWr daynum highlight day_content =
  td
    [ onClick (clickWr daynum)]
    [ span [] [a [href "#", onClick (clickWr daynum)] [text (toString (day daynum))]]
    , day_content
    ]



weekBuild: Context a -> Dict.Dict String (Html a) -> (List Date) -> List (Html a)
weekBuild ctx content obj =
  List.map
    (\d -> (Dict.get (toString d) content)
      |> Maybe.withDefault ctx.default
      |> ctx.day d False) obj


viewMonthWeeks: Context a -> Date -> Dict.Dict String (Html a) -> Html a
viewMonthWeeks ctx month_start content =
  monthList Sun month_start
    |> List.map (weekBuild ctx content)
    |> List.map ctx.week
    |> ctx.month month_start

dateCell: Date -> (Int, Date)
dateCell date =
  (weekNumber date, date)


cmpDateCell: (Int, Date) -> (Int, Date) -> Bool
cmpDateCell (a, _) (b, _) = a == b


monthList: Day -> Date -> List (List Date)
monthList week_start month_start =
  let

    day_of_month_start = D.floor D.Month month_start
    first_day = D.floor D.Sunday day_of_month_start
    eweek = D.ceiling D.Sunday (D.floor D.Month (D.add D.Month 1 month_start))
    caldates = D.range D.Day 1 first_day eweek |> List.map dateCell
  in
    (groupWhile cmpDateCell caldates |> List.map (List.map (\(_,i) -> i)))


basicCalendar =
  { week = simpleTableWeek
  , day = simpleTableDay
  , month = simpleStandardMonth
  , highlight = (\m d y -> False) 
  , dayOfWeek = weekDayToIntUS
  , default = div [] []
  }
 





