module Editable exposing (..)

import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


import Json.Decode
import Json.Decode exposing ((:=))
import Json.Encode
import String
import Semantic as S


type Msg
  = Set String
  | Blur


textbox' v =
  textarea [value v, onInput Set, onBlur Blur] []
 
input' v =
  input [value v, onInput Set, onBlur Blur] []

type Editable a
  = Value a
  | ActiveEdit String (Maybe a)
  | DecodeError String String


update: (String -> Result String a) -> Msg -> Editable a -> Editable a
update decode msg model =
  case msg of
    Set s -> ActiveEdit s Nothing
    Blur ->
      case model of
        Value v -> Value v
        ActiveEdit s v ->
          case decode s of
            Ok v -> Value v
            Err e -> DecodeError s e
        DecodeError s e ->
          DecodeError s e


updateRaw = update Ok

editInput:  (origT -> String) -> Editable origT -> Html Msg
editInput encode model =
  case model of
    Value v ->
      input' (encode v)
    ActiveEdit s _ ->
      input' s
    DecodeError s e ->
      input' s


withInput: (Msg -> a) -> (origT -> String) -> Editable origT -> Html a
withInput wrap ctx model=
  Html.App.map wrap (editInput ctx model)


withTextarea: (Msg -> a) -> (origT -> String) -> Editable origT -> Html a
withTextarea wrap encode model =
  case model of
    Value v ->
      Html.App.map wrap <| textbox' (encode v)
    ActiveEdit s _ ->
      Html.App.map wrap <| textbox' s
    DecodeError s e ->
      Html.App.map wrap <| textbox' s



quickField: ((Msg -> a) -> (origT -> String) -> Editable origT -> Html a) ->
            S.FieldLabel ->
            (Msg -> a) ->
            (origT -> String) ->
            Editable origT ->
            Html a
quickField withChild label wrap encode model =
  let
    emsg = case model of
             DecodeError _ e -> Just e
             _ -> Nothing
    fld = { label = label
          , error_text = emsg
          , content = withChild wrap encode model
          , width = Nothing
          }
  in
    S.viewField fld


stringField lbl updfn value = quickField withInput lbl updfn identity value
floatField lbl updfn value = quickField withInput lbl updfn toString value
inputField = quickField withInput
textareaField = quickField withTextarea


editableToString: (a -> String) -> Editable a -> String
editableToString encode e =
  case e of
    Value v -> encode v
    ActiveEdit s _ -> s
    DecodeError s _ -> s


asText : (a -> String) -> Editable a -> Html b
asText encode e =
  case e of
    Value v -> text (encode v)
    ActiveEdit s _ -> text s
    DecodeError s _ -> text s


fromJson dec = Json.Decode.map Value dec

getJson k dec = (k := Json.Decode.map Value dec)


toJson jsonenc v =
  case v of
    Value v' -> jsonenc v'
    _ -> Json.Encode.null

toJsonStr = toJson Json.Encode.string

editableToResult v =
  case v of
    Value v' -> Ok v'
    _ -> Err "invalid"


maybeResult d v =
  Maybe.withDefault (Ok d) (Maybe.map editableToResult v)
