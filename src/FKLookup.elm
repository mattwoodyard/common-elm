module FKLookup exposing (..)


import Json.Decode as Json
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)

-- import Util as U
-- import Http

-- TODO - rename autocomplete




iconLink icon msg =
  a
    [ href "#", onClick msg]
    [ i [ class <| "ui icon " ++ icon] []]


txtLink txt msg = a [ href "#" , onClick msg ] [text txt]

type Model elemT = NoValue
                 | Valid String elemT
                 | Searching String (Maybe Int) (List elemT)

toResult: Model elemT -> Result String elemT
toResult model =
  case model of
    Valid _ e -> Ok e
    Searching _ _ _ -> Err "Not selected"
    NoValue -> Err "No Assigned Value"

toMaybe: Model elemT -> Maybe elemT
toMaybe model =
  case model of
    Valid _ e -> Just e
    _ -> Nothing

empty = Searching "" Nothing []

type Msg elemT = Search String
               | Select elemT
               | Choose
               | ChooseUp
               | ChooseDown
               | Clear
               | UpdateChoices (List elemT)
               | NoOp

type alias Context elemT =
  { search: (String -> Cmd (Msg elemT))
  , strGetter: (elemT -> String)
  }

keyCodes: Int -> Msg a
keyCodes k =
  case k of
    13 -> Choose
    40 -> ChooseDown
    38 -> ChooseUp
    27 -> Clear
    i ->  NoOp


onKeyDown tagger =
  on "keydown" (Json.map tagger keyCode)


lookupInput search_string =
  div [class "ui category search item"] [
    div [class "ui icon input"]
      [ input
          [ value search_string
          , onKeyDown keyCodes
          , onInput Search
          , class "input"
          ]
          []
      , i [class "ui icon search"] []
      ]
    ]


-- TODO - item rendering should be in context
elemAC ctxt selected idx elem =
  if idx == selected then
    div [ class "item active selected"] [ txtLink (ctxt.strGetter elem) (Select elem)]
  else
    div [ class "item"] [ txtLink (ctxt.strGetter elem) (Select elem)]
 
-- TODO - styling 
viewAC ctxt search_string selected elems =
  menu (lookupInput search_string) (List.indexedMap (elemAC ctxt selected) elems)


icon n = i [ class <| "ui icon " ++ n ] []

menu prompt elems =
  div [class "ui simple dropdown item"]
    [ prompt
    , div [ class "menu visible " ] elems ]

clickToEdit content msg =
  div [class "ui category search item"] [
    div [class "ui icon input"]
      [ txtLink content msg
      , iconLink "search" msg
      ]
    ]



view: Context elemT -> Model elemT -> Html (Msg elemT)
view ctxt model =
  case model of
    NoValue  -> menu (clickToEdit "" (Search "")) []
    Valid txt elem -> menu (clickToEdit txt (Search txt)) []
    Searching srch (Just idx) elems -> viewAC ctxt srch idx elems
    Searching srch Nothing elems -> viewAC ctxt srch -1 elems




updateNoValue lookupDefn msg =
  case msg of
    Search txt ->
      (Searching txt Nothing [] , lookupDefn.search txt)
    _ ->
      NoValue ! []


update: Context elemT -> Msg elemT -> Model elemT -> (Model elemT, Cmd (Msg elemT))
update lookupDefn msg model =
  case (msg, model) of
    (_, NoValue) ->
      updateNoValue lookupDefn msg

    (Search txt, _) ->
      (Searching txt Nothing [], lookupDefn.search txt)

    (Select elem, _) ->
      (Valid (lookupDefn.strGetter elem) elem, Cmd.none)

    (Choose, Valid _ _) ->
      (model, Cmd.none)

        -- TODO fix these
    (Choose, Searching txt (Just chosen) items) ->
      let
        r = List.head (List.drop chosen items)
          |> Maybe.map (\i -> (Valid (lookupDefn.strGetter i) i))
          |> Maybe.withDefault model
      in
        (r, Cmd.none)

    (Choose, Searching txt Nothing items) ->
      (model, Cmd.none)

    (ChooseUp, Valid _ _) ->
      (model, Cmd.none) 
 
    (ChooseUp, Searching txt (Just chosen) items) ->
      if (chosen - 1) >= 0 then
        (Searching txt (Just (chosen - 1)) items, Cmd.none)
      else
        (Searching txt (Just chosen) items, Cmd.none)

    (ChooseUp, Searching txt Nothing items) ->
      (Searching txt (Just 0) items, Cmd.none)

    (ChooseDown, Valid _ _) ->
      (model, Cmd.none)

    (ChooseDown, Searching txt (Just chosen) items) ->
      if (chosen + 1) < List.length items then
        (Searching txt (Just (chosen + 1)) items, Cmd.none)
      else
        (Searching txt (Just chosen) items, Cmd.none)

    (ChooseDown, Searching txt Nothing items) ->
      (Searching txt (Just 0) items, Cmd.none)

    (Clear, Valid _ _) ->
      (model, Cmd.none)

    (Clear, Searching txt _ _) ->
      (Searching txt Nothing [], Cmd.none)

    -- TOOD make the model more refined
    (UpdateChoices l, Searching txt _ _) ->
      (Searching txt (Just 0) l, Cmd.none)

    (UpdateChoices l, Valid txt _) ->
      (Searching txt (Just 0) l, Cmd.none)

    (NoOp, _) -> (model, Cmd.none)


