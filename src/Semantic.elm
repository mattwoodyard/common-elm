module Semantic exposing (..)

import String
-- import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


type FieldLabel = Horizontal String | Vertical String | NoLabel

type alias Field a =
  { label : FieldLabel
  , error_text: Maybe String
  , content: Html a
  , width: Maybe String
  }


renderField: Maybe String -> FieldLabel -> Maybe String -> Html a -> Field a
renderField width flabel errfn content =
  { label = flabel
  , error_text = errfn
  , content = content
  , width = width
  }

viewField: Field a -> Html a
viewField field =
  let
    modw = Maybe.map (\i -> i ++ " ") field.width |> Maybe.withDefault ""
    mode = Maybe.map (\i -> "error ") field.error_text |> Maybe.withDefault ""
    mod = modw ++ mode

  in
    case field.label of
      NoLabel ->
        div [class (mod ++ "field")] [field.content]
      Horizontal labeltxt ->
        div
          [class (mod ++ "inline field")]
          [label [] [text labeltxt], field.content]
      Vertical labeltxt ->
        div
          [class (mod ++ "field")]
          [label [] [text labeltxt], field.content]

fieldGroup grp = div [class "ui fields"] grp

-- renderGroup : Maybe String -> List (Field a) -> Html a
-- renderGroup txt items =
-- div [class "fields"] ([]) ++ List.map viewField

simpleLabelledFieldH l f = renderField Nothing (Horizontal l) Nothing f |> viewField
simpleLabelledFieldV l f = renderField Nothing (Vertical l) Nothing f |> viewField
unlabelledField f = renderField Nothing NoLabel Nothing f |> viewField

labelledH l eTxt fldV = renderField Nothing (Horizontal l) eTxt fldV |> viewField
labelledV l eTxt fldV = renderField Nothing (Vertical l) eTxt fldV |> viewField
unlabelled eTxt fldV = renderField Nothing NoLabel eTxt fldV |> viewField


form errs gps =
  case errs of
    Nothing -> div [class "ui form"] gps
    Just emsgs -> div [class "ui error form"] ([errorMessages emsgs] ++ gps)

errorMessages msgs =
  div
    [ class "ui error message"]
    [ div
      [ class "header"]
      [ text "Form Error"
      , ul
          [ class "list"]
          (List.map (\i -> li [] [text i]) msgs)
      ]
    ]

inplacePopup prompt body =
  case body of
    Nothing ->
      div [ class " ui simple dropdown item"] [prompt ]
    Just body_ ->
      div [ class " ui simple dropdown item"] [prompt, div [class "menu visible"] [body_] ]


type ButtonLabel
  = Text String
  | Icon String
  | TextIcon String String
  | IconText String String

btn : ButtonLabel -> a -> Html a
btn label action =
  case label of
    Text s -> button [class "ui primary button", onClick action] [text s]
    Icon icon_id ->
      button
        [class "ui icon button", onClick action]
        [i [class  (icon_id ++  " icon")] []]
    TextIcon s icon_id ->
      button
        [class "ui right labeled icon button", onClick action]
        [text s, i [class  (icon_id ++  " right icon")] []]
    IconText icon_id s ->
      button
        [class "ui left labeled icon button", onClick action]
        [text s, i [class  (icon_id ++  " left icon")] []]

buttonBar = div [class "ui icon tiny buttons"]

iconBtn icon = btn (Icon icon) 

choiceBtn (atext, aaction) (btext, baction) =
  div
    [ class "ui buttons" ]
    [ button [ class "ui button", onClick aaction ]
      [ text atext ]
    , div [ class "or" ]
      []
    , button [ class "ui button", onClick baction ]
      [ text btext ]
    ]


type SegmentProperties
  = SegAttached
  | SegTop
  | SegBottom
  | SegRightFloated
  | SegPadded

segmentPropAsString prop =
  case prop of
    SegAttached -> "attached"
    SegTop -> "top"
    SegBottom -> "bottom"
    SegRightFloated -> "right floated"
    SegPadded -> "padded"

segmentPropsToCss props =
  (String.join " " <| List.map segmentPropAsString props) ++ " segment"


segment : List SegmentProperties -> List (Html a) -> Html a
segment props content =
  div
    [class ("ui " ++ (segmentPropsToCss props))]
    content

segments = div [class "ui segments"]

toolbaredSegment tools content =
  segments
    [ segment [SegTop, SegAttached] tools
    , segment [SegPadded, SegBottom] content
    ]

cssBuild : (a -> String) -> String -> List a -> String
cssBuild cnvt stype props =
  "ui " ++ (String.join " " <| List.map cnvt props) ++ " " ++ stype

-- buttonCss : ButtonProperties -> String
-- buttonCss prop =
--   case prop of
--     ButtonCommon attr ->
--       attr

type SemanticProps
  = SemAttr String

type SemanticNode a
  = Rendered (Html a)
  | Rendereds (List SemanticProps) (List (Attribute a)) (List (Html a))
  | Semantic (List SemanticProps) (List (Attribute a)) (List SemanticNode a)



render: SemanticNode a -> Html a
render node =
  case node of
    Semantic props attrs children ->
      div ((class "") :: attrs) []
    Rendered content ->
      content
    Rendereds props attrs content ->
      div ((class "") :: attrs) content

