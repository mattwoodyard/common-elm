module CalendarInput exposing (..)
import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Date

import String

import Calendar
import Date.Extra as D
-- import TextInput as TI
import Date exposing (Date, day, month, year)

import Tuple
import Semantic as S
import Dict

import Debug

type Msg = SetByPicker Date
         | BeginSetPicker
         | BeginSetText
         | NextMonth
         | PrevMonth
         | NextYear
         | PrevYear
  --       | TypedInputMsg (TI.Msg)
         | Cancel


type Model = Valid Date
           -- selected date, viewing date
           | Picker Date Date
   --        | TypedInput (TI.Model Date)

toResult : String -> Model -> Result String Date
toResult emsg m =
  case m of
    Valid d -> Ok d
    Picker d1 d2 -> Ok d1
    -- TypedInput inp -> TI.toResult emsg inp

encodeDate : Date -> String
encodeDate d =
  D.toIsoString d


encodeDay: Date -> String
encodeDay d =
  D.toFormattedString "y-MM-dd" d

decodeDate s =
  Debug.log ("decode " ++ (toString s)) D.fromIsoString s |> Maybe.map Ok |> Maybe.withDefault (Err ("Problem Parsing " ++ s))

-- initDate d = Valid d
-- dateField = TI.Context encodeDate decodeDate TI.Blur
-- simpleDisplayDateField = TI.Context encodeDay decodeDate TI.Blur


-- update: Msg -> Model -> (Model, Cmd Msg)
-- update msg model =
--   case (msg, model) of
--     (TypedInputMsg inmsg, TypedInput inp) ->
--       let
--         (m, evt) = TI.update dateField inmsg inp
--       in
--         case m of
--           TI.Valid v -> (Valid v, Cmd.map TypedInputMsg evt)
--           a -> (TypedInput a, Cmd.map TypedInputMsg evt)

--     (TypedInputMsg inmsg, _ ) -> (model, Cmd.none)
--     (BeginSetPicker, Valid selected_date) ->
--       (Picker selected_date selected_date, Cmd.none)

--     -- No Ops for completeness
--     (BeginSetText, Valid d) ->
--       (TypedInput (TI.Valid d), Cmd.none)

--     (SetByPicker ndate, _) ->
--       (Valid ndate, Cmd.none)

--     (PrevMonth, Picker s view) ->
--       (Picker s (D.add D.Month -1 view), Cmd.none)

--     (NextMonth, Picker s view) ->
--       (Picker s (D.add D.Month 1 view), Cmd.none)

--     _ -> Debug.log "Unimplemented noop" (model, Cmd.none)


-- viewCalBox sdate vdate =
--   let
--     sdated = sdate
--     vdated = vdate
--   in
--     div
--       [ class "ui simple dropdown item" ]
--       [ Calendar.viewMonthWeeks selectionCalendar vdated Dict.empty
--       ]

-- inputBox idate = 
--    div [class "ui input icon"]
--         [ input
--             [ value (simpleDisplayDateField.encode idate)
--             , onClick BeginSetText
--             , class "ui input"
--             -- , disabled True
--             ] []
--         , i [ class "ui link icon calendar"
--             , onClick BeginSetPicker
--             ] []
--         ]

-- fieldContent : Model -> Html Msg
-- fieldContent model =
--   case model of
--     Picker sdate vdate ->
--       Just (viewCalBox sdate vdate) |>
--       S.inplacePopup (inputBox sdate)
--     TypedInput ch ->
--       Html.App.map TypedInputMsg (TI.viewInput' simpleDisplayDateField ch)
--     Valid idate ->
--       S.inplacePopup (inputBox idate) Nothing


-- monthHeaderWithNav monthV weeks =
--   let
--     mstring = (toString (Date.month monthV)) ++ " " ++ (toString (Date.year monthV))
--   in
--     table
--       [ class "ui celled center seven column unstackable table day"]
--       [ thead []
--         [ tr []
--           [ th [ colspan 7]
--             [ span
--               [ class "ui prev link"
--               , onClick PrevMonth
--               ]
--               [ i [class "ui icon chevron left"] []]
--             , span [class "link"] [text mstring ]
--             , span
--               [ class "ui next link"
--               , onClick NextMonth
--               ]
--               [ i [class "ui icon chevron right"] []]
--             ]
--           ]
--         , tr []
--           [ th [] [ text "S" ]
--           , th [] [ text "M" ]
--           , th [] [ text "T" ]
--           , th [] [ text "W" ]
--           , th [] [ text "R" ]
--           , th [] [ text "F" ]
--           , th [] [ text "S" ]
--           ]
--         ]

--       , tbody [] weeks
--       ]

-- selectionCalendar =
--   { week = Calendar.simpleTableWeek
--   , day = Calendar.clickableTableDay SetByPicker
--   , month = monthHeaderWithNav
--   , highlight = (\m d y -> False)
--   , dayOfWeek = Calendar.weekDayToIntUS
--   , default = div [] []
--   }


-- conversionTest dstr =
--   let
--     r1 = decodeDate dstr 
--     r2 = Date.fromString dstr
--     r3 = Result.map encodeDate r1
--     r4 = Result.map (\i -> (toString (Date.day i)) ++ "::" ++ (toString (Date.month i))) r2


--     r = dstr ++ " => " ++ (toString r1) ++ "," ++ (toString r2) ++ "," ++ (toString r3) ++ (toString r4)
--   in
--     div [] [text r]




