module RPC exposing (..)


import Http
import Task
import Json.Decode
import Json.Encode



type Response outT
  = SvcOk outT
  | SvcError Http.Error


type Query = Empty

type SvcMethod
  = Get
  | Put
  | Post
  | Delete


type alias Context inT outT =
  { endpoint_url: Maybe inT -> String
  , encode: (inT -> Json.Encode.Value)
  , decode: Json.Decode.Decoder outT
  }


patch : Context inT outT -> inT -> Cmd (Response outT)
patch ctx req =
  Http.post
    ctx.decode
    (ctx.endpoint_url <| Just req)
    (Http.string (Json.Encode.encode 0 (ctx.encode req)))
    |> Task.perform SvcError SvcOk


execute : Context inT outT -> inT -> Cmd (Response outT)
execute ctx req =
  Http.post
    ctx.decode
    (ctx.endpoint_url <| Just req)
    (Http.string (Json.Encode.encode 0 (ctx.encode req)))
    |> Task.perform SvcError SvcOk


query : Context inT outT -> Maybe inT -> Cmd (Response (List outT))
query ctx req =
  Http.get
    (Json.Decode.list ctx.decode)
    (ctx.endpoint_url req)
    |> Task.perform SvcError SvcOk




