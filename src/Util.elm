module Util exposing (..)


import String
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Task
--import Tuple as T
import CmdTuple

import Json.Decode
-- import Json.Decode exposing ((:=))

{-
Some helpers for dealing with the update tuple

-}
pairMap0: (a -> b) -> (a, c) -> (b, c)
pairMap0 f t =
  let
    (a1, c1) = t
  in
    (f a1, c1)

pairMap1: (b -> c) -> (a, b) -> (a, c)
pairMap1 f t =
  let
    (a1, c1) = t
  in
    (a1, f c1)


pairMap: (a -> b) -> (c -> d) -> (a, c) -> (b, d)
pairMap f1 f2 t =
  let
    (a1, c1) = t
  in
    (f1 a1, f2 c1)

updateMap f1 f2 t = pairMap f1 (Cmd.map f2) t
updateModel f1 m f2 t = pairMap (f1 m) (Cmd.map f2) t
noUpdate m v = m



{- Update a model and map a Cmd pair to the updated model
  and a batched cmd
-}

-- updateModel2 : (b -> a -> b) -> b -> (d->c) -> (e->c) -> (a, (Cmd d, Cmd e)) -> (b, Cmd c)
-- updateModel2 f1 m f2 f3 t =
--   T.map2 (f1 m) (CmdTuple.map2 f2 f3) t
--     |> T.map2 identity (Cmd.batch << T.toList2)



iconLink icon msg =
  a
    [ href "#", onClick msg]
    [ i [ class <| "ui icon " ++ icon] []]


txtLink txt msg = a [ href "#" , onClick msg ] [text txt]
txtCell txt = td [] [text txt]
linkCell txt msg = td [] [ txtLink txt msg ]



-- sendMsg m = Task.succeed m |> Task.perform identity identity

validationR2: Result String a -> Result String b -> Result (List String) (a, b)
validationR2  one two =
  case (one, two) of
    (Ok f, Ok s) -> Ok (f, s)
    (Err f, Ok _) -> Err [f]
    (Ok _, Err f) -> Err [f]
    (Err e, Err f) -> Err [e, f]

validationR3: Result String a -> Result String b -> Result String c -> Result (List String) (a, b, c)
validationR3 one two three =
  let
    r1 = validationR2 one two
  in
    case (r1, three) of
      (Ok (a, b), Ok c) -> Ok (a, b, c)
      (Err f, Ok _) -> Err f
      (Ok _, Err f) -> Err [f]
      (Err e, Err f) -> Err (e ++ [f])

validationR4 one two three four =
  let
    r1 = validationR2 one two
    r2 = validationR2 three four
  in
    case (r1, r2) of 
      (Ok (v1, v2), Ok (v3, v4)) -> Ok (v1, v2, v3, v4)
      (Err f, Ok _) -> Err f
      (Ok _, Err f) -> Err f
      (Err e, Err f) -> Err (e ++ f)


 
validationR5 one two three four five  =
  let
    r1 = validationR4 one two three four
  in
    case (r1, five) of
      (Ok (a, b, c, d), Ok e) -> Ok (a, b, c, d ,e)
      (Err f, Ok _) -> Err f
      (Ok _, Err f) -> Err [f]
      (Err e, Err f) -> Err (e ++ [f])

 
validationR6 one two three four five six  =
  let
    r1 = validationR5 one two three four five
  in
    case (r1, six) of
      (Ok (a, b, c, d, e), Ok f) -> Ok (a, b, c, d, e, f)
      (Err f, Ok _) -> Err f
      (Ok _, Err f) -> Err [f]
      (Err e, Err f) -> Err (e ++ [f])


 
validationR7 one two three four five six seven  =
  let
    r1 = validationR6 one two three four five six
  in
    case (r1, six) of
      (Ok (a, b, c, d, e, f), Ok g) -> Ok (a, b, c, d, e, f, g)
      (Err f, Ok _) -> Err f
      (Ok _, Err f) -> Err [f]
      (Err e, Err f) -> Err (e ++ [f])



validationAcc: (a -> Result String b) ->  a -> Result String (List b) -> Result String (List b)
validationAcc resFn item acc =
  case (acc, resFn item) of
    (Ok prev, Ok cur) ->
      Ok(cur :: prev)
    (_, Err e) ->
      Err e 
    (Err e, _) ->
      Err e

validateList: (a -> Result String b) -> List a -> Result String (List b)
validateList resFn xs =
  List.foldl (validationAcc resFn) (Ok []) xs


validationFlatten validation =
  case validation of
    Err e -> Err <| String.join "\n" e
    Ok a -> Ok a


jsonMaybe k enc v =
  case v of
    Just y -> [(k, enc y)]
    _ -> []

jsonAttr k enc v = [(k, enc v)]

-- jsonOptional k parse =
--   Json.Decode.maybe (k := parse)



 
