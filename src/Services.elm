module Services exposing (..)


import Http


type alias ServiceResponse a = Result a Http.Error


type ServiceMsg a b c
  = Request a 
  | Response b
  | Error Http.Error c

