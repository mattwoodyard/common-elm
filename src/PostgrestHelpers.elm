module PostgrestHelpers exposing (..)
import Dict
import Json.Encode
import Json.Decode
-- import Json.Decode exposing ((:=))
-- import Util as U

-- import RPC as RPC
import Http
import Task exposing (Task)


type IntPk = Ok Int | New Int

local i = New i
remote i = Ok i


nextId m =
  case m.nextid of
    Ok i -> Ok (i + 1)
    New i -> New (i + 1)

idToString x =
  toString (idToInt x)

idToInt x =
  case x of
    New i -> -1 * i
    Ok i -> i



-- TODO parse String
idFromInt x =
  if x < 0 then
    New(-1 * x)
  else
    Ok(x)

indexed : List {a|id: IntPk} -> Dict.Dict Int {a |id: IntPk}
indexed xs =
  List.map (\i -> (idToInt i.id, i)) xs
    |> Dict.fromList

type SaveMethod = Patch | Post

type alias Context a b =
  { endpoint_url: (List (String, String) ->  IntPk -> String)
  , encode: (a -> Json.Encode.Value)
  , decode: Json.Decode.Decoder b
  , id: (a -> IntPk)
  , field_map: List (String, String)
  }


{-
Json encode a foreign key reference as an integer or null

-}
maybeId: Maybe {obj|id: Int} -> Json.Encode.Value
maybeId m =
  case m of
    Nothing -> Json.Encode.null
    Just i -> Json.Encode.int i.id


-- save: Context a -> (b -> c) -> a -> Cmd c
-- save context resultHandle model =
--   case context.selector model of
--     Post ->

type alias Identified a = {a|id: IntPk}

-- type SvcMsg a = Store a
--               | Delete a
--               | StoreErr Http.Error
--               | DeleteOk
--               | DeleteErr Http.Error
--               | StoreResultOkIdOnly Int
--               | StoreResultOk a
--               | Get IntPk
--               | GetOk (Maybe a)
--               | GetErr Http.Error

type SvcDetail = Partial | Full


-- noSvcOp: (a, Cmd b) -> (a, (Cmd b, Cmd c))
-- noSvcOp msg =
--   U.pairMap identity (\i-> (Cmd.map identity i, Cmd.none)) msg


-- svcStore msg m =
--   U.pairMap identity (\i-> (Cmd.map identity i, U.sendMsg (Req <| Store m))) msg



-- savePatch ctx obj =
--   patch
--     (Json.Decode.list ctx.decode)
--     (ctx.endpoint_url ctx.field_map (ctx.id obj))
--     (Http.string (Json.Encode.encode 0 (ctx.encode obj)))
--     |> Task.perform RPC.SvcError (RPC.SvcOk << List.head)

-- savePost ctx obj =
--   post
--     ctx.decode
--     (ctx.endpoint_url ctx.field_map (ctx.id obj))
--     (Http.string (Json.Encode.encode 0 (ctx.encode obj)))
--     |> Task.perform RPC.SvcError (RPC.SvcOk << Just)


-- saveDb: Context a b -> a  -> Cmd (RPC.Response (Maybe b))
-- saveDb ctx obj =
--   case ctx.id obj of
--     New _ ->
--       savePost ctx obj
--     Ok _ ->
--       savePatch ctx obj

-- bodyRequest : String -> Json.Decode.Decoder value -> String -> Http.Body -> Task Http.Error value
-- bodyRequest method decoder url body =
--   let request =
--         { verb = method
--         , headers = [ ("Content-type", "application/json"),
--                       ("Prefer", "return=representation")]
--         , url = url
--         , body = body
--         }
--   in
--       Http.fromJson decoder (Http.send Http.defaultSettings request)


-- patch = bodyRequest "PATCH"
-- post = bodyRequest "POST"


-- justGet ctx url =
--   case url of
--     Just url' ->
--          Http.get (Json.Decode.list ctx.decode) url' |> Task.map List.head
--     Nothing -> Task.succeed Nothing

-- -- postAndGetLocation ctx url body =
-- --   let
-- --     request =
-- --       { verb = "POST"
-- --       , headers = [("Content-type", "application/json")]
-- --       , url = url
-- --       , body = body
-- --       }
-- --     response = Http.send Http.defaultSettings request
-- --   in
-- --     Task.map (\r -> Dict.get "Location" r.headers) (Task.mapError promoteError response)
-- --       `Task.andThen` (justGet ctx)
-- --         |> Task.perform (respErr GetFailure) (respOk GetSuccess)



-- promoteError : Http.RawError -> Http.Error
-- promoteError rawError =
--   case rawError of
--     Http.RawTimeout -> Http.Timeout
--     Http.RawNetworkError -> Http.NetworkError


-- url_helper nm flds objid =
--   case objid of
--     Ok i -> Http.url nm ([("id", "eq." ++ (toString i))] ++ flds)
--     _ -> Http.url nm flds


encodePk p =
  case p of
    Ok id -> [ ("id", Json.Encode.int id) ]
    New _ -> []

-- decodePk =
--   ("id" := Json.Decode.map Ok Json.Decode.int)

