module CmdTuple exposing (..)




import Tuple as T


--map2 f1 f2 = T.map2 (Cmd.map f1) (Cmd.map f2)

map2 f1 f2 (a, b) = (Cmd.map f1 a, Cmd.map f2 b)


