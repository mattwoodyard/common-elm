module Layout exposing (..)
import Http
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)




stylesheet : String -> Html a
stylesheet url =
  node "link"
    [ rel "stylesheet"
    , href url ] []

orBlank vw obj = Maybe.map vw obj |> Maybe.withDefault (div [] [])

twoPaneView w1 w2 list detail =
  div
    [ class "ui grid" ]
    [ div [class (w1 ++ " wide column")] [list]
    , div [class (w2 ++ " wide column")] [detail]
    ]

defaultTwoPaneView = twoPaneView "four" "twelve"

