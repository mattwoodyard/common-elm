module Collection exposing (..)


import PostgrestHelpers as PG

import Dict

import Http
import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

import Semantic as S



type EditAmount = Multiple | Single | None

type alias Collection origT editT =
  { items: List origT
  , editting: Dict.Dict String editT
  , nextid: PG.IntPk
  }

type alias Context origT editT =
  { makeEdit: origT -> Maybe editT
  , unEdit: editT -> Result (List String) origT
  , editorCardinality: EditAmount
  }

type Msg origT editT childMsgT
  = Create
  | BeginEdit PG.IntPk
  | Save
  | Cancel
  | ChildAction PG.IntPk childMsgT
  | Remove PG.IntPk


type CrudMsg objT objMsg
  = Add
  | Edit PG.IntPk objMsg


setItems model items =
  {model | items = items}


update : Msg origT editT childMsgT ->  Collection origT editT -> (Collection origT editT, Cmd a)
update msg model =
  model ! []


-- Collection Item helpers
getById model id =
  List.filter (\i -> i.id == id) model.items |> List.head

getByIndex model id =
  List.drop model id |> List.head

replaceByIndex model id nobj =
  List.indexedMap (\i o -> if i == id then nobj else o) model

replaceById xs id nobj =
  List.map (\i -> if i.id == id then nobj else i) xs

replace model id nobj =
  {model| items = replaceById model.items id nobj}

insert model nobj =
  {model | items = model.items ++ [nobj model.nextid]
         , nextid = PG.nextId model
  }




maybGetById model mid =
  case mid of
    Just idx ->
      getById model idx
    Nothing ->
      Nothing


empty = Collection [] Dict.empty (PG.New 0)



addButton target =
  div
    [class "item"]
    [ div [class "right floated"] [ S.btn (S.TextIcon "Add" "add") target]
    , div [class "content"] [   ]
    ]


  
renderDivRows target edit_target model_view model =
  div
    [class "ui divided list"]
    ([addButton target] ++ (List.map (model_view edit_target) model.items))



-- General crud add function
-- add an item created by contstructor
-- to the collection and store in the update 
-- crudAdd : Collection origT editT -> (PG.IntPk -> origT) -> 

-- if edit is selected then
-- validate it, if its valid 
-- apply it to the updater
-- crudSave : Maybe a -> (a -> Result String b) ->
crudSave: Maybe inT ->
          (inT -> Result (List String) b) ->
          (inT -> (c, Cmd d)) ->
          (Maybe (List String) -> (c, Cmd d)) ->
          (c, Cmd d)
crudSave edit validate updater errFn =
  case edit of
    Just toup ->
      case validate toup of
        Ok _ ->
          updater toup
        Err e -> errFn <| Just e
    Nothing ->
      errFn Nothing



