module Semantic.Button exposing (..)

import String
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

import Semantic.Icon as I
import Semantic.Util exposing ((|:))
import Semantic.Util as U

-- Left / Right is where the icon is in relation to the text
-- Left x nothing is label only
type ButtonLabel
  = Left String (Maybe I.Icon)
  | Right I.Icon (Maybe String)

type alias Button a =
  { label: ButtonLabel
  , action: a
  , style: List String}

cssButton = U.cssBuild "button"

render : Button a -> Html a
render btn =
  case btn.label of
    -- left icon right text
    Left txt (Just icon) ->
      button [ U.withStyle ["icon"] btn |> cssButton] []
    -- label only
    Left txt Nothing ->
      button [cssButton btn]  [text txt]
    Right icon (Just lbl) ->
      button [U.withStyle ["icon", "right", "labeled"] btn |> cssButton, onClick btn.action] [text lbl, I.render icon]
    Right icon Nothing ->
      button [U.withStyle ["icon"] btn |> cssButton, onClick btn.action] [I.render icon]


iconButton : I.Icon -> a -> Button a
iconButton icon msg =
  { label = Right icon Nothing
  , action = msg
  , style = []
  }


labelAndIcon : String -> I.Icon -> a -> Button a
labelAndIcon txt icon msg =
  { label = Left txt (Just icon)
  , action = msg
  , style = []
  }

iconAndLabel : I.Icon -> String-> a -> Button a
iconAndLabel icon txt msg =
  { label = Right icon (Just txt)
  , action = msg
  , style = []
  }


rightToolbar : List (Button a) -> Html a
rightToolbar bs =
  div
    [class "right floated"]
    (List.map render bs)

leftToolbar : List (Button a) -> Html a
leftToolbar bs =
  div
    [class "right floated"]
    (List.map render bs)




