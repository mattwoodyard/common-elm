module Semantic.Segments exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)



renderItem: (Maybe (Html a), Html a) -> Html a
renderItem (segheader, segbody) =
  case segheader of
    Nothing ->
      div [class "ui segment"] [segbody]
    Just head -> 
      div [class "ui segment"] [head, div [class "ui divider"] [], segbody]

renderSegmentedCollection: List ((Maybe (Html a), Html a)) -> Html a
renderSegmentedCollection xs =
  div []
  (List.map renderItem xs)
