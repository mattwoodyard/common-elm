module Semantic.Icon exposing (..)

import String
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)

type IconDirection = Up | Down | Left | Right

type Icon = Icon (List String)


cssBuild kind attrs = 
  class ("ui " ++ kind ++" " ++ (String.join " " attrs))

directionString d =
  case d of
    Up -> "up"
    Down -> "down"
    Left -> "left"
    Right -> "right"

chevron d = Icon ["chevron", directionString d]
edit = Icon ["edit"]
add = Icon ["add"]
remove = Icon ["remove"]
save = Icon ["save"]
checkmark = Icon ["checkmark"]
close = Icon ["close"]
home = Icon ["home"]



render: Icon -> Html a
render (Icon def) =
  i [cssBuild "icon" def] []
