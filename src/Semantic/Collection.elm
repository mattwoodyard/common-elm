module Semantic.Collection exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Semantic.Button as Btn


type alias Collection a =
  { tools: List (Btn.Button a)
  , title: String
  , children: Children a
  , content: Html a
  , style: List String
  , link: Maybe String
  }

type Children a = Children (List (Collection a))


renderChild: Collection a -> Maybe (Html a)
renderChild c =
    let

      children = case c.children of
                 Children c -> c

      header_out = case c.link of
                       Just l -> a [href l, class "ui header"] [text c.title]
                       _ -> div [class "ui header"] [text c.title]
    in
      Just (
        div
          [ class (String.join " " (["ui"] ++ c.style ++ ["list"]))]
          [ div
            [ class "ui item"]
            ([Btn.rightToolbar c.tools] ++
             [ 
              div
              [class "ui content"]
              ([header_out, c.content] ++ List.filterMap renderChild children)
             ]
            )
          ]
      )


renderCollection: List String -> List (Collection a) -> Html a
renderCollection styles children =
  div
    [ class (String.join " " (["ui"] ++ styles ++ ["list"]))]
    [ div
      [ class "ui item"]
      [ div
        [class "ui content"]
        (List.filterMap renderChild children)
      ]
    ]


