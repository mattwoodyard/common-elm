module Semantic.Grid exposing (..)

import String
import Html.App exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)




type alias Grid a =
  { content: List (List (Html a))
  , style: List String
  }


render: Grid a -> Html a
render grid =
  div [] []
