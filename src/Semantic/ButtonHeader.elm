module Semantic.ButtonHeader exposing (..)
import Html exposing (..)

import Html.Attributes exposing (..)
import Html.Events exposing (..)

import Semantic.Button as Btn

type ButtonHeader a
  = ButtonHeader String (List (Button a))




render: ButtonHeader a -> Html a
render (ButtonHeader txt buttons) =
  List.map Btn.render buttons 

