module Semantic.Util exposing (..)

import String
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)



cssBuild : String -> Styled b -> Html.Attribute a
cssBuild kind attrs =
  class ("ui " ++ kind ++" " ++ (String.join " " attrs.style))


type alias Styled a = {a| style : List String}

(|:) : Styled a -> String -> Styled a
(|:) styled style =
  {styled| style = styled.style ++ [style] }

withStyle: List String -> Styled a -> Styled a 
withStyle styles styled =
  {styled| style = styled.style ++ styles}

